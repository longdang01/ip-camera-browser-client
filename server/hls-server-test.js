const http = require('http');
const fs = require('fs');
const path = require('path');

const PORT = 4000;
const videoPath = './videos/ipcam/index.m3u8'; // Update with your video file path

http.createServer((req, res) => {
  const filePath = path.resolve(videoPath);
  const stat = fs.statSync(filePath);
  const fileSize = stat.size;

  res.writeHead(200, {
    'Content-Length': fileSize,
    'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'OPTIONS, POST, GET',
        'Access-Control-Max-Age': 2592000, // 30 days
    // 'Content-Type': 'video/mp4',
  });

  const fileStream = fs.createReadStream(filePath);
  fileStream.pipe(res);

  fileStream.on('error', (err) => {
    console.error('Error streaming video:', err);
    res.end();
  });
}).listen(PORT, () => {
  console.log(`Server listening on PORT ${PORT}`);
});
